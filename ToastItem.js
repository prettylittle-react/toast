import React from 'react';
import PropTypes from 'prop-types';

import {UiClose} from 'ui';

import {Content, Editable, Heading, Img, RichText, Text} from 'editable';

import {getModifiers} from 'libs/component';

import './ToastItem.scss';

/**
 * ToastItem
 * @description [Description]
 * @example
  <div id="ToastItem"></div>
  <script>
    ReactDOM.render(React.createElement(Components.ToastItem, {
        title : 'Example ToastItem'
    }), document.getElementById("ToastItem"));
  </script>
 */
class ToastItem extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			isDismissable: props.isDismissable,
			title: props.title,
			timer: props.timer,
			type: props.type,
			isActive: props.isActive
		};

		this.baseClass = 'toast-item';
	}

	get isActive() {
		return this.state.isActive;
	}

	onAfterUpdate() {
		const {timer} = this.state;

		if (this.tick) {
			clearTimeout(this.tick);
			this.tick = null;
		}

		if (timer) {
			this.tick = setTimeout(() => {
				this.tick = null;
				this.close();
			}, timer);
		}
	}

	close() {
		this.setState(
			{
				isActive: false
			},
			() => {
				const {onClose} = this.props;

				if (onClose) {
					onClose();
				}
			}
		);
	}

	onClose = ev => {
		this.close();
	};

	componentDidMount() {
		this.onAfterUpdate();
	}

	componentWillUnmount() {}

	componentWillReceiveProps(nextProps) {
		const changes = {};

		if (nextProps.type !== this.state.type) {
			changes.type = nextProps.type;
		}

		if (nextProps.timer !== this.state.timer) {
			changes.timer = nextProps.timer;
		}

		if (nextProps.isActive !== this.state.isActive) {
			changes.isActive = nextProps.isActive;
		}

		if (nextProps.isDismissable !== this.state.isDismissable) {
			changes.isDismissable = nextProps.isDismissable;
		}

		changes.title = nextProps.title || '';

		/*
		if (nextProps.title !== this.state.title) {
			changes.title = nextProps.title;
		}
		*/

		this.setState(changes, () => {
			this.onAfterUpdate();
		});
	}

	render() {
		const {children, id} = this.props;
		const {type, isActive, timer, isDismissable, title} = this.state;

		if (children === null || isActive === false) {
			return null;
		}

		return (
			<div
				id={id}
				className={getModifiers(this.baseClass, [type])}
				ref={component => (this.component = component)}
			>
				{timer && <div className={`${this.baseClass}__timer`} />}

				{title && (
					<div className={`${this.baseClass}__title`}>
						<Text content={title} />
						{isDismissable && <UiClose className={'close'} onClick={this.onClose} />}
					</div>
				)}

				{title ? (
					<div className={`${this.baseClass}__body`}>{children}</div>
				) : (
					<div className={`${this.baseClass}__main`}>
						<div className={`${this.baseClass}__body`}>{children}</div>
						{isDismissable && <UiClose className={'close'} onClick={this.onClose} />}
					</div>
				)}
			</div>
		);
	}
}

ToastItem.defaultProps = {
	title: '',
	id: null,
	onClose: null,
	isDismissable: false,
	timer: null,
	type: 'default',
	isActive: true,
	children: null
};

ToastItem.propTypes = {
	title: PropTypes.string,
	id: PropTypes.string.isRequired,
	isDismissable: PropTypes.bool,
	timer: PropTypes.number,
	type: PropTypes.string,
	isActive: PropTypes.bool,
	onClose: PropTypes.func,
	children: PropTypes.node
};

export default ToastItem;
