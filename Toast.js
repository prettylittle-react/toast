import React from 'react';
import PropTypes from 'prop-types';

import EventEmitter from 'eventemitter';

import Portal from 'portal';
import ToastItem from './ToastItem';

import './Toast.scss';

let counter = 0;

/// https://jsfiddle.net/69z2wepo/7764/

/**
 * Toast
 * @description [Description]
 * @example
  <div id="Toast"></div>
  <script>
    ReactDOM.render(React.createElement(Components.Toast, {
        title : 'Example Toast'
    }), document.getElementById("Toast"));
  </script>
 */
class Toast extends React.Component {
	constructor(props) {
		super(props);

		this.baseClass = 'toast';

		this.state = {
			items: []
		};
	}

	componentDidMount() {
		EventEmitter.subscribe('toast', this.onToast);
		EventEmitter.subscribe('toast-remove', this.onToastRemove);
	}

	onToastRemove = id => {
		// TODO: remove the toast
	};

	onToast = (children, options = {}) => {
		const {items} = this.state;

		const key = options.id || `toast-${++counter}`;

		const obj = {
			...options,
			...{
				isActive: true,
				id: key,
				key,
				children
			}
		};

		let foundIndex = null;

		items.forEach((item, index) => {
			if (item.id === obj.id) {
				foundIndex = index;
			}
		});

		if (foundIndex !== null) {
			items[foundIndex] = obj;
		} else {
			items.push(obj);
		}

		this.setState({items});
	};

	render() {
		const {items} = this.state;

		const toasts = items.map(item => {
			return <ToastItem {...item} />;
		});

		return <Portal portalId="toast">{toasts}</Portal>;
	}
}

Toast.create = (...args) => {
	EventEmitter.dispatch('toast', ...args);
};

Toast.update = (...args) => {
	EventEmitter.dispatch('toast', ...args);
};

Toast.remove = (...args) => {
	EventEmitter.dispatch('toast-remove', ...args);
};

Toast.defaultProps = {
	children: null
};

Toast.propTypes = {
	children: PropTypes.node
};

export default Toast;
